
<pre>
最近功能增加 支持Spring框架
</pre>
<pre>
功能介绍：http://mp.weixin.qq.com/s/h56mwCvLGK7Jz-725VBIow    和MD写的完全不一样。
当前皆为开发版本。请非开发伙伴不要使用。
https://gitee.com/a_beautiful_butterfly  过去所有版本地址。
https://my.oschina.net/colorleaf 大家有问题或者好的想法，可以到这里留言。
###相见就是缘分，奉送两套bootstarp4模版 https://gitee.com/a_beautiful_butterfly/PiaoLiangDeMoBan
<img src="http://yanshi.sucaihuo.com/modals/32/3277/big.jpg"/><br/>
<br/>


</pre>
该款软件是基于netty开发的分布式后端软件.只有jar方式存在,不依靠tomcat，jetty

支持功能

1通用的mvc功能,支持文本渲染,VelocityEngine渲染，文件上传和文件下载，session,cookie

2支持服务器间发现，信息交互，负载均衡,为其核心功能(一款专门为分布式而生的服务器)

3支持分布式的任务调度
<pre>
1http部分
package beautiful.butterfly;

import beautiful.butterfly.server.httpserver.mvc.aop.AllowNotLogin;
import beautiful.butterfly.server.httpserver.mvc.core.ActionContext;
import beautiful.butterfly.server.httpserver.mvc.core.Controller;
import beautiful.butterfly.server.httpserver.mvc.core.annotation.actionbind.ActionKey;
import beautiful.butterfly.server.httpserver.mvc.multipart.FileItem;
import beautiful.butterfly.server.httpserver.mvc.render.Render;
import beautiful.butterfly.server.httpserver.mvc.render.TextRender;
import beautiful.butterfly.server.httpserver.mvc.render.VelocityRender;
import io.netty.handler.codec.http.Cookie;
import io.netty.handler.codec.http.DefaultCookie;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

@ActionKey("/index")
public class Test extends Controller {
    @AllowNotLogin
    public Render hello() {


        System.out.println("cookie" + ActionContext.getRequest().getClientNameToCookieMap());
        setSessionAttribute("xxx", "9999");
        System.out.println(getSessionAttribute("xxx"));
        Cookie cookies = new DefaultCookie("okm", "xx" + System.currentTimeMillis());
        ActionContext.getRequest().setCookie(cookies);
        return new TextRender("hello");
    }

    @AllowNotLogin
    public Render world() {
        setAttribute("hello", "你好");
        setAttribute("world", "世界");
        List<String> list = new ArrayList<String>();
        list.add("测试1");
        list.add("测试2");
        setAttribute("list", list);
        return new VelocityRender("/hellovelocity.vm");
    }


    @AllowNotLogin
    public Render index() {
        return new VelocityRender("/index.vm");
    }


    @AllowNotLogin
    public Render file() throws Exception {
        FileItem fileItem = ActionContext.getRequest().getFileNameToFileItemMap().get("ok");
        if (fileItem != null) {
            System.out.println(fileItem.getFileName());
            ;
            File file = new File("xxxx.tt");
            System.out.println(file.getAbsolutePath());
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(fileItem.getData(), 0, fileItem.getData().length);
            fileOutputStream.flush();
            fileOutputStream.close();
        }
        return new TextRender("ok");
    }
}
</pre>

<pre>
    .bat方式启动
    @echo off
    SET  now_path=%cd%
    set path= %path%;%now_path%\lib;%now_path%\service_lib;
    set classpath= %classpath%;%now_path%\lib;%now_path%\service_lib;
    set JAVA_OPTS=-Xms128m -Xmx256m
    java -jar original-server-0.0.0-RELEASE.jar
    echo 'my qq is 330937205,if you need help,please contact me'
    pause
</pre>




<pre>
    我先说明下该程序支持idea 直接导入运行beautiful.butterfly.server.application.Main,或者使用maven打包后点击server.bat进行运行即可两种方式。
</pre>



<img src="http://a2.qpic.cn/psb?/V10hn11L3MlBMZ/W6LLvzUTwRfWyc2BCiiCov4XZdvmEzGH*AgcDZjIEnw!/c/dFUBAAAAAAAA&ek=1&kp=1&pt=0&bo=YAKhAAAAAAARF.M!&vuin=330937205&tm=1519448400&sce=60-2-2&rf=0-0"/>




<pre>
