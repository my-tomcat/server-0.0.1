package beautiful.butterfly.server.httpserver.mvc.http;


import beautiful.butterfly.server.httpserver.mvc.render.Render;
import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.cookie.Cookie;

import java.util.Map;

public interface Response {

    /**
     * Judge whether the current getHttpResponse has been submitted to the client
     */
    boolean isCommit();

    int getStatusCode();


    Response setStatusCode(int status);

    String getContentType();

    Response setContentType(CharSequence contentType);


    Response setHeader(CharSequence name, CharSequence value);

    Map<String, String> getHeaderMap();


    Response addCookie(Cookie cookie);

    Response addCookie(String name, String value);


    Response addCookie(String name, String value, int maxAge);


    Response addCookie(String name, String value, int maxAge, boolean secured);


    Response addCookie(String path, String name, String value, int maxAge, boolean secured);


    Response removeCookie(String name);

    Map<String, String> getCookieMap();

    void render(Render render);

    void redirect(String newUri);


    FullHttpResponse send(FullHttpResponse fullHttpResponse);

    public void body(ByteBuf byteBuf);

    public void html(String html);
}
