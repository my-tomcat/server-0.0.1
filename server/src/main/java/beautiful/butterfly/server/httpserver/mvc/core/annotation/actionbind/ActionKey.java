package beautiful.butterfly.server.httpserver.mvc.core.annotation.actionbind;

import java.lang.annotation.*;

/**
 * 支持注解ActionKey
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface ActionKey {

    String value();
}
