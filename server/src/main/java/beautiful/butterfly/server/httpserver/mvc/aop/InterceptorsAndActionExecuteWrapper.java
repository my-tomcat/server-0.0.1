package beautiful.butterfly.server.httpserver.mvc.aop;

import beautiful.butterfly.server.httpserver.mvc.render.Render;

/**
 * 该类主要用于执行拦截器栈中的拦截器
 */
public class InterceptorsAndActionExecuteWrapper extends InterceptorsAndActionExecute {

    private InterceptorsAndActionExecute interceptorsAndActionExecute;
    private Interceptor[] interceptors;


    private int length = 0;

    public InterceptorsAndActionExecuteWrapper(InterceptorsAndActionExecute interceptorsAndActionExecute, Interceptor[] interceptors) {
        this.interceptorsAndActionExecute = interceptorsAndActionExecute;
        this.interceptors = interceptors;
        if (interceptors != null && interceptors.length != 0) {
            this.length = interceptors.length;
        }

    }

    @Override
    public final Render invoke() {
        if (length == 0) {
            return interceptorsAndActionExecute.invoke();
        } else {
            for (int index = 0; index < length; index++) {
                Render render = interceptors[index++].doIt(this);
                if (render != null) {
                    return render;
                }
            }
            return interceptorsAndActionExecute.invoke();

        }

    }
}
