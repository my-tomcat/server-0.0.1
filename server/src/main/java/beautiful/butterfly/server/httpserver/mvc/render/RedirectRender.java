package beautiful.butterfly.server.httpserver.mvc.render;

public class RedirectRender extends Render {
    String url;

    public RedirectRender(String url) {
        this.url = url;
    }

    @Override
    public String render() {
        return url;

    }
}
