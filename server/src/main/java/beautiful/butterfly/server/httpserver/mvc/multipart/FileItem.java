package beautiful.butterfly.server.httpserver.mvc.multipart;


//import beautiful.butterfly.handlers.httpserver.kit.json.JsonIgnore;

import lombok.Data;


@Data
public class FileItem {
    private String name;
    private String fileName;
    private String contentType;
    private long length;
    //@JsonIgnore
    private byte[] data;

    public FileItem(String name, String fileName, String contentType, long length) {
        this.name = name;
        this.fileName = fileName;
        this.contentType = contentType;
        this.length = length;
    }

    /**
     * Get MimeType by ext
     */
    public static String get(String ext) {
        if (MimeType.mimeTypes.containsKey(ext)) {
            return MimeType.mimeTypes.get(ext);
        } else {
            return MimeType.APPLICATION;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public byte[] getData() {
        return data;
    }
    //

    public void setData(byte[] data) {
        this.data = data;
    }
}