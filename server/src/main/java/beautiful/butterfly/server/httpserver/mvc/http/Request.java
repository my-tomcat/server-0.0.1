package beautiful.butterfly.server.httpserver.mvc.http;


import beautiful.butterfly.server.httpserver.mvc.multipart.FileItem;
import io.netty.handler.codec.http.cookie.Cookie;
import lombok.NonNull;

import java.util.List;
import java.util.Map;


public interface Request {


    String getUri();

    String getUrl();

    String getMethod();

    Map<String, List<String>> getParameterMap();

    String getParameter(@NonNull String name);


    public boolean isKeepAlive();


    Map<String, Cookie> getNameToCookieMap();

    Request setCookie(Cookie cookie);

    Map<String, String> getClientNameToCookieMap();

    Session getSession();


    Map<String, FileItem> getFileNameToFileItemMap();


    public String setCookie(String sessionKey);

    Map<String, String> getHeaderMap();

    public String getHeader(String ifModifiedSince);

    Request setAttribute(@NonNull String name, Object value);

    public Object getAttribute(String name);

    Map<String, Object> getAttributeMap();
}
