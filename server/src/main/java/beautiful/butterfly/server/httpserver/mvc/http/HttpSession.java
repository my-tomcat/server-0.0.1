package beautiful.butterfly.server.httpserver.mvc.http;


import lombok.Setter;

import java.util.HashMap;
import java.util.Map;


public class HttpSession implements Session {

    private Map<String, Object> keyToValueMap = new HashMap<>();
    @Setter
    private String id = null;
    @Setter
    private String ip = null;
    @Setter
    private long created = -1;
    @Setter
    private long expired = -1;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getIp() {
        return this.ip;
    }

    @Override
    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public <T> T getAttribute(String name) {
        Object object = this.keyToValueMap.get(name);
        return null != object ? (T) object : null;
    }

    @Override
    public void setAttribute(String name, Object value) {
        this.keyToValueMap.put(name, value);
    }

    @Override
    public Map<String, Object> getAttributeMap() {
        return keyToValueMap;
    }

    @Override
    public void removeAttribute(String name) {
        this.keyToValueMap.remove(name);
    }

    @Override
    public long getCreateTime() {
        return this.created;
    }

    @Override
    public void setCreateTime(long created) {
        this.created = created;
    }

    @Override
    public long getExpireTime() {
        return this.expired;
    }

    @Override
    public void setExpireTime(long expired) {
        this.expired = expired;
    }

}
