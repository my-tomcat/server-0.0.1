package beautiful.butterfly.server.httpserver.mvc.core;

import beautiful.butterfly.server.httpserver.mvc.aop.Interceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Action可以定位到Controller+Method
 */
public class Action {

    public static Logger logger = LoggerFactory.getLogger(Action.class);
    private final String controllerClassKey;
    private final Class<? extends Controller> controllerClass;
    private final Controller controller;
    // 访问URL
    private final String actionKey;
    private final Method method;
    private final String methodName;
    private final List<Interceptor> interceptorList;

    public Action(String actionKey, String controllerKey, Class<? extends Controller> controllerClass,
                  Controller controller, String methodName, Method method, List<Interceptor> interceptorList) {
        this.actionKey = actionKey;
        //
        this.controllerClassKey = controllerKey;
        this.controllerClass = controllerClass;
        this.controller = controller;
        //
        this.methodName = methodName;
        this.method = method;
        /**
         * 如果是以/index/开头则不添加拦截器
         */
        if (actionKey.startsWith("/index/")) {
            interceptorList = new ArrayList<Interceptor>();
        }
        this.interceptorList = interceptorList;

    }

    public String getActionKey() {
        return actionKey;
    }

    public String getControllerKey() {
        return controllerClassKey;
    }

    public Class<? extends Controller> getControllerClass() {
        return controllerClass;
    }

    public Controller getController() {
        return controller;
    }

    public List<Interceptor> getInterceptorList() {
        return interceptorList;
    }

    public String getMethodName() {
        return methodName;
    }

    public Method getMethod() {
        return method;
    }

}
