package beautiful.butterfly.server.httpserver.mvc.core;

import beautiful.butterfly.server.httpserver.mvc.aop.Interceptor;
import beautiful.butterfly.server.httpserver.mvc.aop.InterceptorStack;
import beautiful.butterfly.server.httpserver.mvc.http.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 * Action信息显示器
 */
class ActionReporter {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static Logger log = LoggerFactory.getLogger(ActionReporter.class);

    static final void doReport(Controller controller, Action action) {

        if (controller == null || action == null) {
            if (log.isDebugEnabled()) {
                log.debug("controller == null || action == null");
            }
            return;
        }
        if (ActionContext.getRequest().getFileNameToFileItemMap().size() > 0) {
            return;
        }
        StringBuilder sb = new StringBuilder(
                "\n--------------------------------Action 信息报告 start--------------------------------");
        // 基本信息
        sb.append("\n\n----------信息当前时间:		").append(sdf.format(new Date())).append("    ")
                .append("访问Url:").append("").append("    ")
                .append("Action导航Url:" + action.getActionKey()).append("    referer:")
                .append(action.getController().getHttpRequest().getHeader("referer"));
        Class<? extends Controller> controllerClass = action.getControllerClass();
        sb.append("\n\n----------当前控制器:		").append(controllerClass.getName()).append("	即(")
                .append(controllerClass.getSimpleName()).append(".java文件)");
        sb.append("\n\n----------调用方法:		").append(action.getMethodName());
        // 拦截器信息
        sb.append("\n\n----------该Action拦截器信息显示开始		");
        List<Interceptor> interceptorList = action.getInterceptorList();
        if (interceptorList.size() > 0) {

            for (int i = 0; i < interceptorList.size(); i++) {
                sb.append("\n----------拦截器[" + i + "]:		");
                Interceptor interceptor = interceptorList.get(i);
                if (interceptor != null) {
                    Class<? extends Interceptor> interceptorClass = interceptor.getClass();
                    if (interceptor instanceof InterceptorStack) {
                        sb.append("拦截器栈:");
                    }
                    sb.append(interceptorClass.getName()).append("	即(")
                            .append(interceptorClass.getSimpleName()).append(".java)");
                }

            }
        } else {
            sb.append("\n----------该Action拦截器信息无,如果你在该Action中已经添加拦截器信息，建议你检查拦截器配置是否正确");
        }
        sb.append("\n----------该Action拦截器信息显示结束		");

        {
            // 参数信息
            sb.append("\n\n----------该Action参数信息显示开始		");
            Request request = ActionContext.getRequest();
            @SuppressWarnings("unchecked")
            Set<String> e = request.getParameterMap().keySet();
            if (e.size() == 0) {
                sb.append("\n----------该Action没有提交任何参数		");
            } else {
                Iterator<String> iterator = e.iterator();
                while (iterator.hasNext()) {
                    String name = iterator.next();
                    String[] values = (String[]) request.getParameterMap().get(name).toArray();
                    if (values.length == 1) {
                        sb.append("\n----------只有一个参数:" + name).append("=").append(values[0]);
                    } else {
                        {// 一行显示
                            sb.append("\n----------参数信息[一行]显示:" + name).append("[]={");
                            for (int i = 0; i < values.length; i++) {
                                if (i > 0) {
                                    sb.append("    ,");
                                }
                                sb.append(values[i]);
                            }
                            sb.append("}");
                        }
                        {// 多行显示
                            sb.append("\n----------参数信息[多行]显示如下:" + name).append("[]");
                            for (int i = 0; i < values.length; i++) {
                                sb.append("\n----------" + name + "[" + i + "]		").append(values[i]);
                            }
                        }
                    }
                }
            }

            sb.append("\n----------该Action参数信息显示结束		");
        }

        sb.append("\n--------------------------------Action 信息报告 end--------------------------------");
        if (log.isDebugEnabled()) {
            log.debug(sb.toString());
        }
    }

}
