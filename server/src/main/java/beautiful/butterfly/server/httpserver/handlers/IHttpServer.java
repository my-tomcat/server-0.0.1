package beautiful.butterfly.server.httpserver.handlers;

import beautiful.butterfly.server.application.Application;


public interface IHttpServer {


    void start(Application application) throws Exception;


    void stop();


}
