package beautiful.butterfly.server.httpserver.handlers;

import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;

import java.util.concurrent.ExecutorService;


public class EpollEventLoopGroups {
    public static boolean epollIsAvailable() {
        try {
            Object isAvailable = Class.forName("io.netty.channel.epoll.Epoll").getMethod("isAvailable").invoke(null);
            return null != isAvailable && Boolean.valueOf(isAvailable.toString());
        } catch (Exception e) {
            return false;
        }
    }

    public static Group group(int threadCount, ExecutorService bossExecutors, int workers, ExecutorService workerExecutors) {
        EpollEventLoopGroup bossEventLoopGroup = new EpollEventLoopGroup(threadCount, bossExecutors);
        EpollEventLoopGroup workerEventLoopGroup = new EpollEventLoopGroup(workers, workerExecutors);

        Group group = new Group();
        group.setSocketChannel(EpollServerSocketChannel.class);
        group.setBoosMultithreadEventLoopGroup(bossEventLoopGroup);
        group.setWorkerMultithreadEventLoopGroup(workerEventLoopGroup);
        return group;
    }

}
