package beautiful.butterfly.server.httpserver.mvc.render;

import beautiful.butterfly.server.httpserver.mvc.core.ActionContext;
import beautiful.butterfly.server.httpserver.mvc.http.Request;
import beautiful.butterfly.server.httpserver.mvc.http.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class Render {

    public static Logger logger = LoggerFactory.getLogger(Render.class);

    public Request getRequest() {
        return ActionContext.getRequest();
    }

    public Response getResponse() {
        return ActionContext.getResponse();
    }

    public abstract String render();

}
