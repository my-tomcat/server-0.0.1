package beautiful.butterfly.server.httpserver.mvc.aop;

import beautiful.butterfly.server.httpserver.mvc.render.Render;

/**
 * 继承这个类可以保证Interceptor线程安全
 */
public abstract class PrototypeInterceptor implements Interceptor {

    public final Render doIt(InterceptorsAndActionExecute interceptorsAndActionExecute) {
        try {
            return getClass().newInstance().doPrototypeIntercept(interceptorsAndActionExecute);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public abstract Render doPrototypeIntercept(InterceptorsAndActionExecute interceptorsAndActionExecute);
}
