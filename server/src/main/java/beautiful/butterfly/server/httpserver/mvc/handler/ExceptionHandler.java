package beautiful.butterfly.server.httpserver.mvc.handler;


public interface ExceptionHandler {

    String VARIABLE_STACKTRACE = "stackTrace";

    void handle(Exception e);

}
