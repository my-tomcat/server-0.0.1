package beautiful.butterfly.server.httpserver.mvc.aop;

import java.lang.annotation.*;

/**
 * ClearInterceptor 能够清除施加在该对象上面不同层次的拦截器:Upper或者All.默认Upper
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ClearInterceptor {

    /**
     * 清除拦截器模式只能选择一种
     */
    ClearLayer value() default ClearLayer.Before;
}
