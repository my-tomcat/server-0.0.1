package beautiful.butterfly.server.httpserver.mvc.render;


public class TextRender extends Render {

    private String text;

    public TextRender(String text) {
        this.text = text;
    }

    @Override
    public String render() {
        return text;

    }
}
