package beautiful.butterfly.server.httpserver.mvc;

import beautiful.butterfly.server.httpserver.mvc.http.Session;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class SessionContent {

    private Map<String, Session> sessionIdToSessionMap;

    public SessionContent() {
        this.sessionIdToSessionMap = new ConcurrentHashMap<>();
    }

    public Map<String, Session> getSessionIdToSessionMap() {
        return sessionIdToSessionMap;
    }

    public void addSession(Session session) {
        sessionIdToSessionMap.put(session.getId(), session);
    }

    public void remove(Session session) {
        sessionIdToSessionMap.remove(session.getId());
    }

    public void clear() {
        sessionIdToSessionMap.clear();
    }

    public Session getSession(String id) {
        return sessionIdToSessionMap.get(id);
    }


}
