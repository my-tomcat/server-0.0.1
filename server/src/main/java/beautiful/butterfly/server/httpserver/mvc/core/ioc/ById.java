package beautiful.butterfly.server.httpserver.mvc.core.ioc;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ById {

    String value();

}
