package beautiful.butterfly.server.httpserver.mvc.aop;

import beautiful.butterfly.server.httpserver.mvc.render.Render;

public interface Interceptor {

    public Render doIt(InterceptorsAndActionExecute interceptorsAndActionExecute);
}
