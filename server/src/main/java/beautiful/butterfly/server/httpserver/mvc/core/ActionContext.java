package beautiful.butterfly.server.httpserver.mvc.core;


import beautiful.butterfly.server.httpserver.handlers.HttpServer;
import beautiful.butterfly.server.httpserver.mvc.http.Request;
import beautiful.butterfly.server.httpserver.mvc.http.Response;
import beautiful.butterfly.server.httpserver.mvc.http.Session;

import java.io.UnsupportedEncodingException;


public final class ActionContext {
    private static ThreadLocal<HttpServer> $httpServer = new ThreadLocal<HttpServer>();
    private static ThreadLocal<Request> $request = new ThreadLocal<Request>();
    private static ThreadLocal<Response> $response = new ThreadLocal<Response>();


    public static void setActionContext(HttpServer httpServer, Request request, Response response) {
        $httpServer.set(httpServer);
        //
        $request.set(request);
        $response.set(response);

    }

    //
    public static HttpServer getHttpServer() {

        return $httpServer.get();
    }

    public static Request getRequest() {

        return $request.get();
    }

    public static Response getResponse() {
        return $response.get();
    }

    //
    public static void remove() {
        $httpServer.remove();
        $request.remove();
        $response.remove();

    }


    public static void setAttribute(String name, Object object) {
        ((Request) getRequest()).setAttribute(name, object);
    }

    public static Object getAttribute(String name) {
        return ((Request) getRequest()).getParameterMap().get(name);
    }

    // 基本的三个对象获取
    public static Session getHttpSession() {
        return ((Request) getRequest()).getSession();
    }

    /**
     * 支持中文
     */
    // 提供两个处理get提交Request请求的两个方法
    public static String getParameter(String name) {
        String value = getRequest().getParameter(name);
        if (value == null) {
            return null;
        }
        value = value.trim();
        if ("get".equalsIgnoreCase(getRequest().getMethod().toString())) {
            try {
                return new String(value.getBytes("iso-8859-1"), "utf-8");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        } else {
            return value;
        }
    }

    public static String[] getParameterValues(String name) {
        String[] values = (String[]) getRequest().getParameterMap().get(name).toArray();
        if (values == null) {
            return null;
        }
        if ("get".equalsIgnoreCase(getRequest().getMethod().toString())) {
            if (values != null && values.length == 0) {
                return null;
            } else {
                int length = values.length;
                for (int i = 0; i < length; i++) {
                    try {
                        values[i] = new String(values[i].getBytes("iso-8859-1"), "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        throw new RuntimeException(e);
                    }
                }
                return values;
            }
        } else {
            if (values != null && values.length == 0) {
                return null;
            } else {
                return values;
            }
        }
    }

}
