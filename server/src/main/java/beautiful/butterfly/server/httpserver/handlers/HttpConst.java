package beautiful.butterfly.server.httpserver.handlers;

import beautiful.butterfly.server.httpserver.mvc.Constant;
import io.netty.util.AsciiString;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public interface HttpConst {


    String cookie = "cookie";
    String set_cookie = "set-cookie";


    String IF_MODIFIED_SINCE = "IF_MODIFIED_SINCE";
    String USER_AGENT = "User-Agent";
    String CONTENT_TYPE_STRING = "Content-Type";
    String COOKIE_STRING = "XCookie";
    String METHOD_GET = "GET";
    String DEFAULT_SESSION_KEY = "SESSION";
    String SLASH = "/";

    CharSequence CONNECTION = AsciiString.cached("Connection");
    CharSequence CONTENT_LENGTH = AsciiString.cached("Content-Length");
    CharSequence CONTENT_TYPE = AsciiString.cached("Content-Type");
    CharSequence DATE = AsciiString.cached("Date");
    CharSequence LOCATION = AsciiString.cached("Location");
    CharSequence X_POWER_BY = AsciiString.cached("X-Powered-By");
    CharSequence EXPIRES = AsciiString.cached("Expires");
    CharSequence CACHE_CONTROL = AsciiString.cached("Cache-Control");
    CharSequence LAST_MODIFIED = AsciiString.cached("Last-Modified");
    CharSequence SERVER = AsciiString.cached("IHttpServer");
    CharSequence KEEP_ALIVE = AsciiString.cached("Keep-Alive");

    String CONTENT_TYPE_HTML = "text/html; charset=UTF-8";

    CharSequence VERSION = AsciiString.cached("application-" + Constant.VERSION);

    Map<CharSequence, CharSequence> contentTypes = new ConcurrentHashMap<>(8);


}
