package beautiful.butterfly.server.httpserver.mvc.core;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;


public class WebContent {

    public static Logger logger = LoggerFactory.getLogger(WebContent.class);


    /**
     * 集成Spring
     */
    static ApplicationContext webApplicationContext = null;

    public static ApplicationContext getWebApplicationContext() {
        if (webApplicationContext != null) {
            return webApplicationContext;
        } else {
            ApplicationContext webApplicationContext = new FileSystemXmlApplicationContext("");
            WebContent.webApplicationContext = webApplicationContext;
        }
        return webApplicationContext;
    }


}
