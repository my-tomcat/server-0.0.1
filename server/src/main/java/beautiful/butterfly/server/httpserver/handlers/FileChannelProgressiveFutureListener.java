package beautiful.butterfly.server.httpserver.handlers;

import io.netty.channel.ChannelProgressiveFuture;
import io.netty.channel.ChannelProgressiveFutureListener;
import lombok.extern.slf4j.Slf4j;

import java.io.RandomAccessFile;


@Slf4j
public class FileChannelProgressiveFutureListener implements ChannelProgressiveFutureListener {

    private RandomAccessFile randomAccessFile;

    public FileChannelProgressiveFutureListener(RandomAccessFile randomAccessFile) {
        this.randomAccessFile = randomAccessFile;
    }

    public static FileChannelProgressiveFutureListener build(RandomAccessFile randomAccessFile) {
        return new FileChannelProgressiveFutureListener(randomAccessFile);
    }

    @Override
    public void operationProgressed(ChannelProgressiveFuture channelProgressiveFuture, long progress, long total) {
    }

    @Override
    public void operationComplete(ChannelProgressiveFuture channelProgressiveFuture) {
        try {
            randomAccessFile.close();
        } catch (Exception e) {
            log.error("RandomAccessFile close error", e);
        }
    }

}