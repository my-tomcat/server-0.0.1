package beautiful.butterfly.server.httpserver.handlers;

import beautiful.butterfly.server.httpserver.mvc.http.Request;
import beautiful.butterfly.server.httpserver.mvc.http.Response;
import io.netty.channel.ChannelHandlerContext;


public interface IStaticFileHandler<R> {

    R handle(ChannelHandlerContext channelHandlerContext, Request request, Response response) throws Exception;

}
