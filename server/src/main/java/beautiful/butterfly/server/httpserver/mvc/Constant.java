package beautiful.butterfly.server.httpserver.mvc;


public interface Constant {

    //basic
    String default_server_address = "0.0.0.0";
    int default_server_port = 1024;
    String server_address = "address";
    String server_port = "port";
    //
    String default_static_file_path = "/static";
    String default_templates_path = "/templates";
    String default_file_upload_path = "/file_upload";
    //
    String static_file_path = "static_file_path";
    String templates_path = "templates_path";
    String file_upload_path = "file_upload_path";


    //ssl
    String ssl_enable = "ssl.enable";
    String ssl_cert_path = "ssl.cert-path";
    String ssl_private_key = "ssl.private-key-path";
    String ssl_private_key_password = "ssl.private-key-pass";


    String VERSION = "2.0.5-RELEASE";

    String CONTENT_TYPE_HTML = "text/html; charset=UTF-8";

    String CONTENT_TYPE_TEXT = "text/plain; charset=UTF-8";
    String HTTP_DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss zzz";
    String INTERNAL_SERVER_ERROR_HTML = "<center><h1>500 Internal IHttpServer Error</h1><hr/></center>";


    String ENV_KEY_GZIP_ENABLE = "http.gzip.enable";
    String ENV_KEY_CORS_ENABLE = "http.cors.enable";


    //

    String ENV_KEY_NETTY_BOOS_GROUP_NAME = "netty.boos";
    String ENV_KEY_NETTY_WORKER_GROUP_NAME = "netty.worker";
    String ENV_KEY_NETTY_THREAD_COUNT = "netty.thread-count";
    String ENV_KEY_NETTY_WORKERS = "netty.workers";
    String ENV_KEY_NETTY_SO_BACKLOG = "netty.backlog";


}
