package beautiful.butterfly.server.httpserver.mvc.aop;

import java.lang.annotation.*;

/**
 * 拦截器注解,可以继承
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Before {

    Class<? extends Interceptor>[] value();
}
