package beautiful.butterfly.server.httpserver.kit;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;


public class NamedThreadFactory implements ThreadFactory {

    private final String prefix;
    private final AtomicLong threadNumber = new AtomicLong();

    public NamedThreadFactory(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public Thread newThread(Runnable runnable) {
        threadNumber.addAndGet(1);
        return new Thread(runnable, prefix + " thread-" + threadNumber.intValue());
    }
}