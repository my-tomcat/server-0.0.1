package beautiful.butterfly.server.httpserver.mvc.aop;

import beautiful.butterfly.server.httpserver.mvc.core.Action;
import beautiful.butterfly.server.httpserver.mvc.core.Controller;
import beautiful.butterfly.server.httpserver.mvc.render.Render;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 调用Action的容器:包含拦截器和目的方法<br/>
 * 包括Interceptor[]和Action[Controller.Method]
 */
public class InterceptorsAndActionExecute {

    private static final Object[] args = new Object[0];
    private static Logger logger = LoggerFactory.getLogger(InterceptorsAndActionExecute.class);
    private Controller controller;
    private Action action;
    private Interceptor[] interceptors;
    private int length = 0;
    private int index = 0;

    protected InterceptorsAndActionExecute() {
    }

    public InterceptorsAndActionExecute(Action action, Controller controller) {
        this.controller = controller;

        this.action = action;
        // list转数组
        Interceptor[] _s = new Interceptor[action.getInterceptorList().size()];
        action.getInterceptorList().toArray(_s);
        this.interceptors = _s;

        this.length = _s.length;

    }

    public Action getAction() {
        return action;
    }

    public Render invoke() {

        if (length == 0) {
            try {
                // 调用Action中目的方法
                Object object = action.getMethod().invoke(controller, args);
                if (object == null) {
                    return null;
                } else {
                    Render render = (Render) object;
                    return render;
                }

            } catch (Exception e) {
                logger.error(e.getMessage());

                // 获取最近的异常的原因,所以Action内部要仔细的处理异常
                String exceptionMsg = e.getCause().getMessage();
                logger.error(exceptionMsg);
                throw new RuntimeException(exceptionMsg);
            }


        } else {
            for (int index = 0; index < length; index++) {
                Render render = interceptors[index++].doIt(this);
                if (render != null) {
                    return null;

                }
            }
            try {
                // 调用Action中目的方法
                Object object = action.getMethod().invoke(controller, args);
                if (object == null) {
                    return null;
                } else {
                    Render render = (Render) object;
                    return render;
                }
            } catch (Exception e) {
                logger.error(e.getMessage());

                // 获取最近的异常的原因,所以Action内部要仔细的处理异常
                String exceptionMsg = e.getCause().getMessage();
                logger.error(exceptionMsg);
                throw new RuntimeException(exceptionMsg);
            }


        }

    }

}
