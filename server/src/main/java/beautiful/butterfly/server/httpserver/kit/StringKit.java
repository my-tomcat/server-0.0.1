package beautiful.butterfly.server.httpserver.kit;

import beautiful.butterfly.server.httpserver.mvc.multipart.FileItem;
import lombok.NoArgsConstructor;


@NoArgsConstructor
public final class StringKit {


    public static boolean isNotBlank(String str) {
        return null != str && !"".equals(str.trim());
    }


    public static boolean isBlank(String str) {
        return null == str || "".equals(str.trim());
    }


    public static String fileExt(String fname) {
        if (isBlank(fname) || fname.indexOf('.') == -1) {
            return null;
        }
        return fname.substring(fname.lastIndexOf('.') + 1);
    }

    public static String mimeType(String fileName) {
        String ext = fileExt(fileName);
        if (null == ext) {
            return null;
        }
        return FileItem.get(ext);
    }


}
