package beautiful.butterfly.server.httpserver.mvc.core;


import beautiful.butterfly.server.httpserver.mvc.http.HttpRequest;
import beautiful.butterfly.server.httpserver.mvc.http.HttpResponse;
import beautiful.butterfly.server.httpserver.mvc.http.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;


public abstract class Controller {


    public static Logger logger = LoggerFactory.getLogger(Controller.class);

    public HttpRequest getHttpRequest() {
        return (HttpRequest) ActionContext.getRequest();
    }

    public HttpResponse getHttpResponse() {
        return (HttpResponse) ActionContext.getResponse();
    }

    /**
     * request里面取出数据并且可以进行转换特定的数据类型：Boolean,Byte,Short,Integer,Long,Float,Double<br/>
     * request所有的方法<br/>
     * <p>
     * <pre>
     * {@link #getParameter(String, String)}
     * {@link #getParameterToInt(String)}
     * {@link #getParameterToInt(String, Integer)}
     * {@link #getParameterToInt(String, int)}
     * {@link #getParameterToLong(String)}
     * {@link #getParameterToLong(String, Long)}
     * {@link #getParameterToFloat(String)}
     * {@link #getParameterToFloat(String, Float)}
     * {@link #getParameterToDouble(String)}
     * {@link #getParameterToDouble(String, Float)}
     * </pre>
     */
    public String getParameter(String name) {
        HttpRequest httpRequest = getHttpRequest();
        String value = httpRequest.getParameter(name);
        if (value == null) {
            return null;
        } else {
            return value.trim();
        }

    }

    public String getParameter(String name, String defaultValue) {
        HttpRequest httpRequest = getHttpRequest();
        String result = httpRequest.getParameter(name);
        return result != null ? result.trim() : defaultValue;
    }

    public Double getParameterToDouble(String name) {
        String result = getParameter(name);
        return result != null ? Double.parseDouble(result.trim()) : null;
    }

    public Double getParameterToDouble(String name, Float defaultValue) {
        String result = getParameter(name);
        return result != null ? Double.parseDouble(result.trim()) : defaultValue;
    }

    public Float getParameterToFloat(String name) {
        String result = getParameter(name);
        return result != null ? Float.parseFloat(result.trim()) : null;
    }

    public Float getParameterToFloat(String name, Float defaultValue) {
        String result = getParameter(name);
        return result != null ? Float.parseFloat(result.trim()) : defaultValue;
    }

    public Integer getParameterToInt(String name) {
        String result = getParameter(name);
        return result != null ? Integer.parseInt(result.trim()) : null;
    }

    /**
     * 主要用到的一个函数.针对NullPointerException和NumberFormatException异常进行处理-0
     */
    public Integer getParameterToInt(String name, int defaultValue) {
        try {
            String result = getParameter(name);
            return result != null ? Integer.parseInt(result.trim()) : defaultValue;
        } catch (NullPointerException e) {
            logger.debug(e.getMessage());

            return defaultValue;
        } catch (NumberFormatException e) {
            logger.debug(e.getMessage());
            return defaultValue;
        }
    }

    /**
     * 主要用到的一个函数.针对NullPointerException和NumberFormatException异常进行处理-null
     */
    public Integer getParameterToInt(String name, Integer defaultValue) {
        try {
            String result = getParameter(name);
            return result != null ? Integer.parseInt(result.trim()) : defaultValue;
        } catch (NullPointerException e) {
            return null;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public Long getParameterToLong(String name) {
        String result = getParameter(name);
        return result != null ? Long.parseLong(result.trim()) : null;
    }

    public Long getParameterToLong(String name, Long defaultValue) {
        String result = getParameter(name);
        return result != null ? Long.parseLong(result.trim()) : defaultValue;
    }

    public Long getParameterToLong(String name, long defaultValue) {
        String result = getParameter(name);
        return result != null ? Long.parseLong(result.trim()) : defaultValue;
    }

    /**
     * 请求参数如果值不止一个的情况
     * <p>
     * <pre>
     * {@link #getParameterValuesToInt(String)}
     * {@link #getParameterValuesToLong(String)}
     * </pre>
     */
    public String[] getParameterValues(String name) {
        HttpRequest httpRequest = getHttpRequest();
        return (String[]) httpRequest.getParameterMap().get(name).toArray();
    }


    public Integer[] getParameterValuesToInt(String name) {
        String[] values = getParameterValues(name);
        if (values == null) {
            return null;
        }
        Integer[] result = new Integer[values.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = Integer.parseInt(values[i]);
        }
        return result;
    }

    public Long[] getParameterValuesToLong(String name) {

        String[] values = getParameterValues(name);
        if (values == null) {
            return null;
        }
        Long[] result = new Long[values.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = Long.parseLong(values[i]);
        }
        return result;
    }


    /**
     * session处理
     * <p>
     * <pre>
     * {@link #getSession()}
     * {@link #getSessionAttribute(String)}
     * </pre>
     */


    public Session getSession() {
        HttpRequest httpRequest = getHttpRequest();
        return httpRequest.getSession();
    }

    public Controller setSessionAttribute(String key, Object value) {
        HttpRequest httpRequest = getHttpRequest();
        httpRequest.getSession().setAttribute(key, value);
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> T getSessionAttribute(String key) {
        HttpRequest httpRequest = getHttpRequest();
        Session session = httpRequest.getSession();
        return session != null ? (T) session.getAttribute(key) : null;
    }

    /**
     * string to int
     */
    public int getSessionAttributeToInt(String key, int defaultValue) {
        HttpRequest httpRequest = getHttpRequest();
        Session session = httpRequest.getSession();
        String value = (String) session.getAttribute(key);
        if (value == null) {
            return defaultValue;
        } else {
            try {
                return Integer.parseInt(value);
            } catch (Exception e) {
                return defaultValue;
            }

        }

    }

    public int getSessionAttribute(String key, int defaultValue) {
        HttpRequest httpRequest = getHttpRequest();
        Session session = httpRequest.getSession();
        Integer value = (Integer) session.getAttribute(key);
        try {
            return value.intValue();
        } catch (Exception e) {
            return value.intValue();
        }

    }


    public Controller removeSessionAttribute(String key) {
        HttpRequest httpRequest = getHttpRequest();
        Session session = httpRequest.getSession();
        if (session != null) {
            session.removeAttribute(key);
        }
        return this;
    }

    //
    public Controller setAttribute(String name, Object value) {
        HttpRequest httpRequest = getHttpRequest();
        httpRequest.setAttribute(name, value);
        return this;
    }

    public Object getAttribute(String name) {
        return getHttpRequest().getAttributeMap().get(name);
    }

    public Controller setAttributeMap(Map<String, Object> attributeMap) {
        HttpRequest httpRequest = getHttpRequest();
        for (Map.Entry<String, Object> entry : attributeMap.entrySet()) {
            httpRequest.setAttribute(entry.getKey(), entry.getValue());
        }
        return this;
    }

    public Controller removeAttribute(String name) {
        HttpRequest httpRequest = getHttpRequest();
        if (httpRequest.getParameterMap().get(name) != null) {
            httpRequest.getParameterMap().remove(name);
        }
        return this;
    }


}