package beautiful.butterfly.server.application;

import org.springframework.context.ApplicationContext;

public class ApplicationContextUtils {
    private static Application Application;
    private static ApplicationContext ApplicationContext;

    public static void register(Application application, ApplicationContext applicationContext) {
        Application = application;
        ApplicationContext = applicationContext;
    }

    public static Application getApplication() {
        return Application;
    }

    public static ApplicationContext getApplicationContext() {
        return ApplicationContext;
    }
}
