package beautiful.butterfly.server.application;

import java.io.File;


public class Main {
    static {
        System.out.println("Joey Ma Siu-Ling 来了");
    }

    public static String getRootPath() {
        String packageName = Main.class.getPackage().getName().replace(".", File.separator) + File.separator;
        String path = Main.class.getResource("").getFile();
        path = Environment.pathFix(path);
        path = path.replace(packageName, "");
        if (path.contains(".jar")) {//处理jar文件 两层xx/xx/xx
            int lastIndexOf = path.lastIndexOf(File.separator);
            path = path.substring(0, lastIndexOf);
            lastIndexOf = path.lastIndexOf(File.separator);
            path = path.substring(0, lastIndexOf);
        }
        return path.replace("file:\\", "");//去掉file:\
    }

    public static void main(String[] as) throws Exception {
        String path = getRootPath();//class path跟路径(资源路径的父级路径,非class路径)
        System.out.println("当前系统classpath根路径:" + path);
        Application application = new Application(path);
        application.start();
    }
}
